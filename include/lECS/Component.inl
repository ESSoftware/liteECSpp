namespace lECS
{
    template<typename ComponentType>
    Component<ComponentType>::Component(Entity entity)
    : m_entity(entity)
    {
    }

    template<typename ComponentType>
    Entity Component<ComponentType>::getEntity() const
    {
        return m_entity;
    }

    template<typename ComponentType>
    void Component<ComponentType>::setEntity(Entity entity)
    {
        m_entity = entity;
    }
}
