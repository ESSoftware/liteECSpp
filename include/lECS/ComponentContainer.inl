#include <algorithm>

namespace lECS
{
    template<typename ComponentType>
    template<typename ... Params>
    void ComponentContainer<ComponentType>::add(Entity entity, Params&& ... params)
    {
        m_components.emplace_back(entity, std::forward<Params>(params)...);
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::const_iterator ComponentContainer<ComponentType>::begin() const
    {
        return m_components.begin();
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::const_iterator ComponentContainer<ComponentType>::end() const
    {
        return m_components.end();
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::const_iterator ComponentContainer<ComponentType>::find(Entity entity) const
    {
        return std::find_if(begin(), end(), [entity](ComponentType const& component){return component.getEntity() == entity;});
    }

    template<typename ComponentType>
    void ComponentContainer<ComponentType>::remove(Entity entity)
    {
        m_components.erase(std::remove_if(begin(), end(), [entity](ComponentType const& component){return component.getEntity() == entity;}), end());
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::iterator ComponentContainer<ComponentType>::begin()
    {
        return m_components.begin();
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::iterator ComponentContainer<ComponentType>::end()
    {
        return m_components.end();
    }

    template<typename ComponentType>
    typename ComponentContainer<ComponentType>::iterator ComponentContainer<ComponentType>::find(Entity entity)
    {
        return std::find_if(begin(), end(), [entity](ComponentType const& component){return component.getEntity() == entity;});
    }
}
