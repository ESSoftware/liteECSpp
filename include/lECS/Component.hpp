#ifndef COMPONENTBASE_HPP_INCLUDED
#define COMPONENTBASE_HPP_INCLUDED

#include "lECS/Entity.hpp"

///
///@namespace lECS
///@brief Namespace where all the lECS library is defined
///
namespace lECS
{
    ///
    ///@class Component Component.hpp
    ///@brief Base class for creating components.
    ///@tparam ComponentType Type of the derived component, used to differentiate the base class for each component, so the user can inherit a component from two base components.
    ///This class has to be inherited publicly or at least to expose to a public scope the getEntity() member function if inherited privatly.
    ///
    template<typename ComponentType>
    class Component
    {
    public:
        ///
        ///@brief Get the entity corresponding to this component.
        ///@return The entity corresponding to the component.
        ///
        Entity getEntity() const;

    protected:
        ///
        ///@brief Constructor
        ///@param entity The entity the component has to be assigned to.
        ///
        Component(Entity entity);

        ///
        ///@brief Sets the component entity.
        ///@param The entity the component has to be transfered to.
        ///
        void setEntity(Entity entity);

    private:
        Entity m_entity; ///<\private The component entity.
    };
}

#include "lECS/Component.inl"

#endif // COMPONENTBASE_HPP_INCLUDED
