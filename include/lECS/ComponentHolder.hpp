#ifndef COMPONENTHOLDER_HPP_INCLUDED
#define COMPONENTHOLDER_HPP_INCLUDED

#include <memory>

#include "lECS/Entity.hpp"

namespace lECS
{
    class AComponentContainer;

    template<typename>
    class ComponentContainer;

    class ComponentHolder
    {
    public:
        ComponentHolder() = default;

    private:
        template<typename ComponentType, typename ... Params>
        void add(Entity entity, Params&& ... params);

        template<typename ComponentType>
        void addComponentContainer();

        template<typename ComponentType>
        lECS::ComponentContainer<ComponentType>& getComponentContainer();

        template<typename ComponentType>
        lECS::ComponentContainer<ComponentType> const& getComponentContainer() const;

    private:
        template<typename T>
        using ContainerType = std::vector<T>;

        ContainerType<std::unique_ptr<AComponentContainer>> m_componentContainers;

    private:
        template<typename,typename...>
        friend class System;

        friend class EntityManager;
    };
};

#include "lECS/ComponentHolder.inl"

#endif // COMPONENTHOLDER_HPP_INCLUDED
