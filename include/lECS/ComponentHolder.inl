#include <cassert>

namespace lECS
{
    template<typename ComponentType, typename ... Params>
    void ComponentHolder::add(Entity entity, Params&& ... params)
    {
        addComponentContainer<ComponentType>();

        getComponentContainer<ComponentType>().add(entity, std::forward<Params>(params)...);
    }

    template<typename ComponentType>
    void ComponentHolder::addComponentContainer()
    {
        bool exists{false};
        for(auto  const& element : m_componentContainers)
        {
            if(typeid(*element) == typeid(ComponentContainer<ComponentType>))
                exists = true;
        }

        if(!exists)
        {
            auto ptr = std::make_unique<ComponentContainer<ComponentType>>();
            m_componentContainers.push_back(std::move(ptr));
        }
    }

    template<typename ComponentType>
    ComponentContainer<ComponentType>& ComponentHolder::getComponentContainer()
    {
        for(auto const& element : m_componentContainers)
        {
            if(typeid(*element) == typeid(ComponentContainer<ComponentType>))
                return static_cast<ComponentContainer<ComponentType>&>(*element);
        }

        assert(false && "You should never reach this point.");
    }

    template<typename ComponentType>
    ComponentContainer<ComponentType> const& ComponentHolder::getComponentContainer() const
    {
        for(auto const& element : m_componentContainers)
        {
            if(typeid(*element) == typeid(ComponentContainer<ComponentType>))
                return static_cast<ComponentContainer<ComponentType> const&>(*element);
        }

        assert(false && "You should never reach this point.");
    }
}
