#ifndef ENTITY_HPP_INCLUDED
#define ENTITY_HPP_INCLUDED

namespace lECS
{
    using Entity = uint32_t;
}

#endif // ENTITY_HPP_INCLUDED
