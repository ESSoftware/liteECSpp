#ifndef SYSTEM_HPP_INCLUDED
#define SYSTEM_HPP_INCLUDED

#include "lECS/ComponentHolder.hpp"
#include "lECS/Context.hpp"

namespace lECS
{
    template<typename SystemType, typename ComponentType>
    class SystemBase
    {
    };

    template<typename SystemType, typename ... ComponentTypes>
    class System : private SystemBase<SystemType, ComponentTypes> ...
    {
    protected:
        System(Context& context);

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type find(Entity entity);

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type begin();

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type end();

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type find(Entity entity) const;

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type begin() const;

        template<typename ComponentType>
        typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type end() const;

    private:
        ComponentHolder& m_componentHolder;
    };
}

#include "lECS/System.inl"

#endif // SYSTEM_HPP_INCLUDED
