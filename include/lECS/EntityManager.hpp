#ifndef ENTITYMANAGER_HPP_INCLUDED
#define ENTITYMANAGER_HPP_INCLUDED

#include <stack>

namespace lECS
{
    class EntityManager
    {
    public:
        EntityManager(ComponentHolder& componentHolder);

        Entity getFreeEntity();
        void freeEntity(Entity entity);

        template<typename ComponentType, typename ... Params>
        void addComponent(Entity entity, Params&& ... params);

        template<typename ... ComponentTypes>
        void deleteComponents(Entity entity);

        template<typename...>
        class ComponentDeleter
        {
        };

    private:
        ComponentHolder& m_componentHolder;
        std::stack<Entity> m_freedEntities;
        Entity m_nextFreeEntity;
    };
}

#include "lECS/EntityManager.inl"

#endif // ENTITYMANAGER_HPP_INCLUDED
