#ifndef COMPONENTCONTAINER_HPP_INCLUDED
#define COMPONENTCONTAINER_HPP_INCLUDED

#include <vector>

#include "lECS/Entity.hpp"
#include "lECS/System.hpp"
#include "lECS/EntityManager.hpp"

namespace lECS
{
    class AComponentContainer
    {
    public:
        virtual ~AComponentContainer() = default;

    protected:
        AComponentContainer() = default;
    };

    template<typename ComponentType>
    class ComponentContainer : public AComponentContainer
    {
    private:
        using collection_type = std::vector<ComponentType>;

    public:
        ComponentContainer() = default;

        template<typename ... Params>
        void add(Entity entity, Params&& ... params);

        using const_iterator = typename collection_type::const_iterator;

        const_iterator begin() const;
        const_iterator end() const;
        const_iterator find(Entity entity) const;

    private:
        void remove(Entity entity);

        using iterator = typename collection_type::iterator;

        iterator begin();
        iterator end();
        iterator find(Entity entity);

    private:
        collection_type m_components;

    private:
        template<typename, typename...>
        friend class System;

        friend class EntityManager;
    };
}

#include "lECS/ComponentContainer.inl"

#endif // COMPONENTCONTAINER_HPP_INCLUDED
