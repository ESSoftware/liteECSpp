#ifndef CONTEXT_HPP_INCLUDED
#define CONTEXT_HPP_INCLUDED

#include "lECS/EntityManager.hpp"
#include "lECS/ComponentHolder.hpp"

namespace lECS
{
    class Context
    {
    public:
        Context();

    private:
        ComponentHolder m_componentHolder;

    public:
        EntityManager entityManager;

    private:
        template<typename, typename...>
        friend class System;
    };
}

#include "lECS/Context.inl"

#endif // CONTEXT_HPP_INCLUDED
