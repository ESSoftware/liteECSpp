namespace lECS
{
    template<typename SystemType, typename ... ComponentTypes>
    System<SystemType, ComponentTypes...>::System(Context& context)
    : m_componentHolder{context.m_componentHolder}
    {
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type System<SystemType, ComponentTypes...>::find(Entity entity)
    {
        return m_componentHolder.getComponentContainer<ComponentType>().find(entity);
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type System<SystemType, ComponentTypes...>::begin()
    {
        return m_componentHolder.getComponentContainer<ComponentType>().begin();
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::iterator>::type System<SystemType, ComponentTypes...>::end()
    {
        return m_componentHolder.getComponentContainer<ComponentType>().end();
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type System<SystemType, ComponentTypes...>::find(Entity entity) const
    {
        return m_componentHolder.getComponentContainer<ComponentType>().find(entity);
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type System<SystemType, ComponentTypes...>::begin() const
    {
        return m_componentHolder.getComponentContainer<ComponentType>().begin();
    }

    template<typename SystemType, typename ... ComponentTypes>
    template<typename ComponentType>
    typename std::enable_if<std::is_base_of<SystemBase<SystemType, const ComponentType>, System<SystemType, ComponentTypes...>>::value, typename ComponentContainer<ComponentType>::const_iterator>::type System<SystemType, ComponentTypes...>::end() const
    {
        return m_componentHolder.getComponentContainer<ComponentType>().end();
    }
}
