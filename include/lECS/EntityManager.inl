namespace lECS
{
    EntityManager::EntityManager(ComponentHolder& componentHolder)
    : m_componentHolder{componentHolder}
    , m_freedEntities{}
    , m_nextFreeEntity{}
    {
    }

    Entity EntityManager::getFreeEntity()
    {
        Entity entity{};

        if(!m_freedEntities.empty())
        {
            entity = m_freedEntities.top();
            m_freedEntities.pop();
        }
        else
            entity = m_nextFreeEntity++;

        return entity;
    }

    void EntityManager::freeEntity(Entity entity)
    {
        m_freedEntities.push(entity);
    }

    template<typename ComponentType, typename ... Params>
    void EntityManager::addComponent(Entity entity, Params&& ... params)
    {
        m_componentHolder.add<ComponentType>(entity, std::forward<Params>(params)...);
    }

    template<typename ... ComponentTypes>
    void EntityManager::deleteComponents(Entity entity)
    {
        ComponentDeleter<ComponentTypes...>::deleteComponents(entity, m_componentHolder);
    }

    template<typename FirstComponentType, typename ... ComponentTypes>
    class EntityManager::ComponentDeleter<FirstComponentType, ComponentTypes...>
    {
    public:
        static void deleteComponents(Entity entity, ComponentHolder& componentHolder)
        {
            componentHolder.getComponentContainer<FirstComponentType>().remove(entity);
            ComponentDeleter<ComponentTypes...>::deleteComponents(entity, componentHolder);
        }
    };

    template<>
    class EntityManager::ComponentDeleter<>
    {
    public:
        static void deleteComponents(Entity entity, ComponentHolder& componentHolder)
        {
        }
    };
}
