#ifndef lECS_HPP_INCLUDED
#define lECS_HPP_INCLUDED

#include "lECS/Component.hpp"
#include "lECS/ComponentContainer.hpp"
#include "lECS/ComponentHolder.hpp"
#include "lECS/Context.hpp"
#include "lECS/Entity.hpp"
#include "lECS/EntityManager.hpp"
#include "lECS/System.hpp"

#endif // lECS_HPP_INCLUDED
