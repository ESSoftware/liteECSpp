#include <iostream>

#include "lECS.hpp"

class TestComponent : public lECS::Component<TestComponent>
{
public:
    TestComponent(lECS::Entity entity)
    : Component(entity)
    {
    }

    int myInt{};
};

class OtherComponent : public lECS::Component<OtherComponent>
{
public:
    OtherComponent(lECS::Entity entity)
    : Component(entity)
    {
    }

    std::string myString{"Hello world !"};
};

class TestSystem : public lECS::System<TestSystem, TestComponent, const OtherComponent>
{
public:
    template<typename ... Args>
    TestSystem(Args&& ... args)
    : System(std::forward<Args>(args)...)
    {
    }

    void update()
    {
        for(auto it = begin<TestComponent>(); it != end<TestComponent>(); ++it)
        {
            it->myInt = 5;
            std::cout << "Hello ! My entity is " << it->getEntity() << " and my int is " << it->myInt << std::endl;
            auto otherIt = find<OtherComponent>(it->getEntity());
            if(otherIt != end<OtherComponent>())
                std::cout << "My OtherComponent has a message for you : " << otherIt->myString << std::endl;
        }
    }
};

int main()
{
    lECS::Context context{};

    auto firstEntity = context.entityManager.getFreeEntity();
    context.entityManager.addComponent<TestComponent>(firstEntity);
    context.entityManager.addComponent<OtherComponent>(firstEntity);

    auto secondEntity = context.entityManager.getFreeEntity();
    context.entityManager.addComponent<TestComponent>(secondEntity);
    context.entityManager.addComponent<OtherComponent>(secondEntity);

    TestSystem testSystem{context};

    testSystem.update();

    context.entityManager.deleteComponents<TestComponent, OtherComponent>(firstEntity);
    context.entityManager.freeEntity(firstEntity);

    auto thirdEntity = context.entityManager.getFreeEntity();

    context.entityManager.addComponent<TestComponent>(thirdEntity);
    context.entityManager.addComponent<OtherComponent>(thirdEntity);

    testSystem.update();

    return 0;
}
